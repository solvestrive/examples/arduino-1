static int analogOutValue = 255;
int OUT_PIN = 3;

void setupAo() {
    pinMode(OUT_PIN, OUTPUT);
    analogWrite(OUT_PIN, 0);
}

void switchAoOn() {
    analogWrite(OUT_PIN, analogOutValue);
}

void switchAoOff() {
    analogWrite(OUT_PIN, 0);
}

void setAoValue(int value) {
    analogOutValue = value;
    switchAoOn();
}