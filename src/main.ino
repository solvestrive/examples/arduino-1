#include "diEdgeDetectionDefine.h"

String inString = "";

void setup() {
    Serial.begin(9600);
    setupAo();
    setupDi();
}

void loop() {

    switch (dioEdgeDetection()) {
    case RISING_EDGE:
        switchAoOn();
        break;
    case FALLING_EDGE:
        switchAoOff();
        break;
    default:
        break;
    }
    
    int inChar = Serial.read();
    if (isDigit(inChar)) {
      inString += (char)inChar;
    }
    if (inChar == '\n') {
      Serial.println();
      setAoValue(inString.toInt());
      inString = "";
    }

}
