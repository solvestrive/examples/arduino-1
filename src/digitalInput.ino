#include "diEdgeDetectionDefine.h"

int SWITCH_AO_ON_PIN = 7;

void setupDi() {
    pinMode(SWITCH_AO_ON_PIN, INPUT_PULLUP);
}

int dioEdgeDetection() {
    static int lastState = LOW;
    int state = digitalRead(SWITCH_AO_ON_PIN);
    int edge = NO_CHANGE;

    if (state == HIGH && lastState == LOW) {
        edge = RISING_EDGE;
    } else if (state == LOW && lastState == HIGH) {
        edge = FALLING_EDGE;
    }

    lastState = state;
    return edge;
}