import fs from "fs";

const download = require('download'); // eslint-disable-line
const decompress = require('decompress'); // eslint-disable-line

const file = 'http://solvestrive.com/wp-content/uploads/2021/09/test-runner-0.0.1.zip';
const filePath = `${__dirname}/..`;
const zipFileName = 'test-runner-0.0.1.zip';

download(file, filePath)
.then(() => {
  console.log('Download Completed');

  decompress(`${__dirname}/../test-runner-0.0.1.zip`, `${__dirname}/..`).then(() => {

    if (fs.existsSync(filePath + '/__MACOSX')) {
      fs.rmdirSync(filePath + '/__MACOSX', {recursive: true});
    }

    if (fs.existsSync(filePath + '/' + zipFileName)) {
      fs.unlink(filePath + '/' + zipFileName, () => {
        console.log('done!');
      });
    }

  });
})
