import { SpecFileLoader } from './spec-file-loader';
import { TestRunner } from './test-runner';

async function main(): Promise<void> {
  const fileLoader = new SpecFileLoader();
  const testRunner = new TestRunner();
  const files = fileLoader.loadAllSpecFiles('./fixture');
  let err;
  for(const file of files) {
    try {
      await testRunner.runAllTestsFromFile('../' + file);
    } catch (e) {
      err = e;
    }
  }
  if (err) {
    throw err;
  }
}

main();
