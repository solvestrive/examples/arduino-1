import { readdirSync, statSync } from 'fs';
import { join, parse } from 'path';

export class SpecFileLoader {
  private files: string[] = [];

  loadAllSpecFiles(directory: string): string[] {
    this.files = [];
    this.getFilesRecursively(directory);
    return this.files;
  }

  private getFilesRecursively(directory: string): void {
    readdirSync(directory).forEach(file => {
      const absolute = join(directory, file);
      if (statSync(absolute).isDirectory()) {
        this.getFilesRecursively(absolute);
      } else {
        if (!this.isHiddenFile(parse(file).name)) {
          this.files.push(join(directory, parse(file).name));
        }
      }
    });
  }

  private isHiddenFile(filename: string): boolean {
    return filename.substring(0,1) === '.';
  }

}
