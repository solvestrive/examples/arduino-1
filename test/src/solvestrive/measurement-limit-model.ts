export class MeasurementLimitModel {
  constructor(public lowerLimit: string,
              public upperLimit: string,
              public unit: string) {}
}