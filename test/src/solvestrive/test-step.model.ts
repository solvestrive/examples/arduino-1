import { MeasurementLimitModel } from './measurement-limit-model';

export class TestStepModel {
  constructor(public command: string,
              public description?: string,
              public testStepConfigurationParameter?: Object, // eslint-disable-line
              public measurementLimit?: MeasurementLimitModel) {}
}
