import { TestStepModel } from './test-step.model';
import axios from 'axios';
import * as config from '../../solvestriveconfig.json';

export class SolvestriveFixture {

  private steps: TestStepModel[];
  private results = new Array<string>();
  private host?: string;
  private port?: string;

  constructor() {
    this.steps = [];
    this.host = config.host;
    this.port = config.port;
  }

  async run(): Promise<void> {
    const hostPort = (this.host ? this.host : 'localhost')
        + (this.port ? (':' + this.port) : null);
    const url = 'http://' + hostPort + '/v1/test-runner/run-test';
    const resp = await axios.post<string[]>(url, this.steps);
    this.results = resp.data;
  }

  getResults(): string[] {
    return this.results;
  }

  setDo(device: string, ...pins: string[]): SolvestriveFixture {
    pins.forEach(pin => {
      this.steps.push(new TestStepModel(
          'Dio On',
          'switch dio ' + pin + ' on',
          this.mapToObj(new Map().set('Device', device).set('Pin', pin))))
    });
    return this;
  }

  resetDo(device: string, ...pins: string[]): SolvestriveFixture {
    pins.forEach(pin => {
      this.steps.push(new TestStepModel(
          'Dio Off',
          'switch dio ' + pin + ' off',
          this.mapToObj(new Map().set('Device', device).set('Pin', pin))))
    });
    return this;
  }

  measureVoltage(device: string, pin: string, minVolt: number, maxVolt: number): SolvestriveFixture {
    this.steps.push(new TestStepModel(
        'Analog Input',
        'Measure voltage',
        this.mapToObj(
            new Map().set('Device', device)
            .set('Pin', pin)
            .set('Minimum Value', minVolt)
            .set('Maximum Value', maxVolt))));
    return this;
  }

  wait(waitTimeMs: number): SolvestriveFixture {
      this.steps.push(new TestStepModel(
          'DelayMs',
          'Wait',
          this.mapToObj(new Map().set('Value', waitTimeMs.toString()))));
    return this;
  }

  initUart(port: string, baudrate: number): SolvestriveFixture {
    this.steps.push(new TestStepModel(
        'Uart Init',
        'Initialize Uart',
        this.mapToObj(
            new Map().set('Port', port)
            .set('Baud', baudrate))));
    return this;
  }

  closeUart(port: string): SolvestriveFixture {
    this.steps.push(new TestStepModel(
        'Uart Close',
        'Close Uart',
        this.mapToObj(new Map().set('Port', port))));
    return this;
  }

  writeToUart(port: string, data: string): SolvestriveFixture {
    this.steps.push(new TestStepModel(
        'Uart Write',
        'Write Data to Uart',
        this.mapToObj(
            new Map().set('Port', port)
            .set('Data', data))));
    return this;
  }

  readUartBuffer(port: string): SolvestriveFixture {
    this.steps.push(new TestStepModel(
        'Uart Read',
        'Read Bufer from Uart',
        this.mapToObj(new Map().set('Port', port))));
    return this;
  }

  private mapToObj(strMap: Map<string, string>) {
    const obj = Object.create(null);
    for (const [k,v] of strMap) {
      obj[k] = v; //look out! Key must be a string!
    }
    return obj;
  }
}
