import { parse } from 'path';

export class TestRunner {

  async runAllTestsFromFile(filename: string): Promise<void> {
    let errorOccured = false;
    console.log("running test from file " + parse(filename).name);

    const file = require(filename); // eslint-disable-line
    const keys = Object.keys(file);
    for(const key of keys) {
      console.log("running test " + key);
      try {
        await file[key]();
      } catch (e) {
        console.log("ERROR: test failed at: " + key);
        errorOccured = true;
      }
    }

    console.log("\r");
    if (errorOccured) {
      throw 'error occured at ' + parse(filename).name;
    }
  }

}
