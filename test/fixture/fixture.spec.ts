import { SolvestriveFixture } from '../src/solvestrive/solvestrive-fixture';
import * as assert from "assert";

const arduinoComPort = 'COM4';
const niDaqDevice = 'Dev1';
const aoActivationPin = '0.1';

async function init(fixture: SolvestriveFixture) {
  fixture.initUart(arduinoComPort, 9600);
  fixture.resetDo(niDaqDevice, aoActivationPin);
  fixture.wait(5000); // wait for arduino to init after reset because of opening com port
}

export async function switchOnAoTo5V_whenSwitchOnDi7ForFirstTime() {
  let testFixture = new SolvestriveFixture();
  await init(testFixture);
  
  testFixture.measureVoltage(niDaqDevice, '1', 0, 5);
  testFixture.setDo(niDaqDevice, aoActivationPin);
  testFixture.wait(2000);
  testFixture.measureVoltage(niDaqDevice, '1', 0, 5);

  testFixture.closeUart(arduinoComPort);
  await testFixture.run();
  console.log(testFixture.getResults());

  const lowLevelValue: number = Number(testFixture.getResults()[0].replace('\,', '\.'));
  assert.equal(lowLevelValue < 0.15, true);
  const highLevelValue: number = Number(testFixture.getResults()[1].replace('\,', '\.'));
  assert.equal(highLevelValue > 4.9 && highLevelValue < 5.1, true);
}

export async function setAoTo2_5V_whenDi7SwitchedOnAndSending127ViaUart() {
  let testFixture = new SolvestriveFixture();
  await init(testFixture);
  
  testFixture.measureVoltage(niDaqDevice, '1', 0, 5);
  testFixture.setDo(niDaqDevice, aoActivationPin);
  testFixture.wait(2000);
  testFixture.measureVoltage(niDaqDevice, '1', 0, 5);
  testFixture.writeToUart(arduinoComPort, '127\n');
  testFixture.wait(2000);
  testFixture.measureVoltage(niDaqDevice, '1', 0, 5);

  testFixture.closeUart(arduinoComPort);
  await testFixture.run();
  console.log(testFixture.getResults());

  const lowLevelValue: number = Number(testFixture.getResults()[0].replace('\,', '\.'));
  assert.equal(lowLevelValue < 0.15, true);
  const highLevelValue: number = Number(testFixture.getResults()[1].replace('\,', '\.'));
  assert.equal(highLevelValue > 4.9 && highLevelValue < 5.1, true);
  const setByUartLevelValue: number = Number(testFixture.getResults()[2].replace('\,', '\.'));
  assert.equal(setByUartLevelValue > 2.48 && setByUartLevelValue < 2.51, true);
}

export async function switchOffAo_whenDioSwitchedOff() {
  let testFixture = new SolvestriveFixture();
  await init(testFixture);
  
  testFixture.measureVoltage(niDaqDevice, '1', 0, 5);
  testFixture.setDo(niDaqDevice, aoActivationPin);
  testFixture.wait(2000);
  testFixture.measureVoltage(niDaqDevice, '1', 0, 5);
  testFixture.resetDo(niDaqDevice, aoActivationPin);
  testFixture.wait(2000);
  testFixture.measureVoltage(niDaqDevice, '1', 0, 5);

  testFixture.closeUart(arduinoComPort);
  await testFixture.run();
  console.log(testFixture.getResults());

  const lowLevelValue: number = Number(testFixture.getResults()[0].replace('\,', '\.'));
  assert.equal(lowLevelValue < 0.15, true);
  const highLevelValue: number = Number(testFixture.getResults()[1].replace('\,', '\.'));
  assert.equal(highLevelValue > 4.9 && highLevelValue < 5.1, true);
  const switchedOffLevelValue: number = Number(testFixture.getResults()[2].replace('\,', '\.'));
  assert.equal(switchedOffLevelValue < 0.15, true);
}

export async function setAoToLastValue_whenDioSwitchedOnASecondTime() {
  let testFixture = new SolvestriveFixture();
  await init(testFixture);
  
  testFixture.setDo(niDaqDevice, aoActivationPin);
  testFixture.wait(500);
  testFixture.writeToUart(arduinoComPort, '127\n');
  testFixture.resetDo(niDaqDevice, aoActivationPin);
  testFixture.wait(500);
  testFixture.setDo(niDaqDevice, aoActivationPin);
  testFixture.wait(2000);
  testFixture.measureVoltage(niDaqDevice, '1', 0, 5);

  testFixture.closeUart(arduinoComPort);
  await testFixture.run();
  console.log(testFixture.getResults());

  const setByUartLevelValue: number = Number(testFixture.getResults()[0].replace('\,', '\.'));
  assert.equal(setByUartLevelValue > 2.48 && setByUartLevelValue < 2.51, true);
}

export async function setAoTo3_75V_whenDi7SwitchedOnAndSending191ViaUart() {
  let testFixture = new SolvestriveFixture();
  await init(testFixture);
  
  testFixture.measureVoltage(niDaqDevice, '1', 0, 5);
  testFixture.setDo(niDaqDevice, aoActivationPin);
  testFixture.writeToUart(arduinoComPort, '191\n');
  testFixture.wait(2000);
  testFixture.measureVoltage(niDaqDevice, '1', 0, 5);

  testFixture.closeUart(arduinoComPort);
  await testFixture.run();
  console.log(testFixture.getResults());

  const setByUartLevelValue: number = Number(testFixture.getResults()[1].replace('\,', '\.'));
  assert.equal(setByUartLevelValue > 3.65 && setByUartLevelValue < 3.85, true);
}
