# script-generator

## setup
- install all node dependencies ` npm i `
- download and set up the SolveStrive test runner. ` npm run install-testrunner `


## start the test runner and run a test run
- start the test-runner. ` npm run run-test-runner `
- start a test-run. ` npm run run-ts `
