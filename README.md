# arduino-1 example

## Project description

This project is a sample project to explain how you can automate the systemtests of your hardware using SolveStrive.
In this example we are using a [Arduino Uno](https://store.arduino.cc/arduino-uno-rev3) board and
a [NI-USB 6001 DAQ Card](https://www.ni.com/de-at/support/model.usb-6001.html).

The arduino is the DUT (device under test) which should implement the requirements.
The behaviour of the system (arduino + additional hardware + software) will be tested with the NI-USB 6001 DAQ Card.

## Getting started

Clone the project: ``` git clone https://gitlab.com/solvestrive/examples/arduino-1.git ```

Upload the main.io to your Arduino Uno.

Change to the test directory of the project ``` cd arduino-1/test ```

Install all node dependencies ``` npm i ```

Download and set up the SolveStrive test runner. ``` npm run install-testrunner ```

Start the test-runner. ``` npm run run-test-runner ```

Run the tests. ``` npm run run-ts ```

## Hardware

- [Arduino Uno](https://store.arduino.cc/arduino-uno-rev3)
- [NI-USB 6001](https://www.ni.com/de-at/support/model.usb-6001.html)
- resistor 1k
- capacitor 220µF

### Schematics

![schematics](schematics.jpg "schematics")

## Software

- [Java11](https://www.oracle.com/java/technologies/javase-jdk11-downloads.html)
- [NI-DAQmx driver + NI-MAX](https://www.ni.com/de-at/support/downloads/drivers/download.ni-daqmx.html#409845)
- [LabVIEW Runtime 2020 64bit](https://www.ni.com/de-at/support/downloads/software-products/download.labview.html#352879)
- [Arduino IDE](https://www.arduino.cc/en/software)
- [Git](https://gitforwindows.org/)
- [VS Code](https://code.visualstudio.com)
- [Node.js](https://nodejs.org/en/)
- [Notepad++](https://notepad-plus-plus.org/downloads/)

## Project requirements

### Already implemented

- [x] The analog output at pin 3 should be switched on when the digital input 7 is set to high. 
  Otherwise, it should be off.
- [x] The initial value when the analog output is switched on at the first time must be 5V.
- [x] The analog output value can be set via the uart.
    - The Values must be sent as String.
    - The end of the data must be marked wit a "\n".
    - The value mapping is 0 -> 0V and 255 -> 5V. Every value between 0 and 255 is calculated as x*(5V/255)
    
### For future implementation (for training)

- [ ] The value of the analog output can only be changed when the digital input 7 is switched on.
